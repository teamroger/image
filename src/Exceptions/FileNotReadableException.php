<?php declare(strict_types=1);

namespace Image\Exceptions;

class FileNotReadableException extends ImageException
{

}
