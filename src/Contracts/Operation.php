<?php declare(strict_types=1);

namespace Image\Contracts;

interface Operation
{
    public function apply($imageResource);
}
