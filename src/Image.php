<?php declare(strict_types=1);

namespace Image;

use Image\Contracts\Operation;
use Image\Exceptions\FileNotFoundException;
use Image\Exceptions\BadFileFormatException;
use Image\Exceptions\FileNotReadableException;

/**
 * Models an image file.
 * Provides functionality to get attributes of the image such as width and
 * height and also to manipulate the image through image operations.
 *
 * This class is abstract and must be extended such that the sub-class
 * implements the image-specific functionality.
 *
 * @author Roger Barnfather
 */
abstract class Image
{
    protected $filename;
    protected $image;

    /**
     * Opens an image file.
     * If an image is already open for this object then it will
     * be closed just after successfully opening the new one.
     *
     * @param $filename string The file to open.
     * @return \Image\Image This object.
     */
    protected function open(string $filename) : Image
    {
        if (!file_exists($filename)) {
            throw new FileNotFoundException(
                'File "' . $filename . '" does not exist'
            );
        }
    
        if (!is_readable($filename)) {
            throw new FileNotReadableException(
                'File "' . $filename . '" is not readable'
            );
        }

        // Delagate the specific opening method to the sub-class.
        $newImageResource = $this->createFromFile($filename);

        if ($this->image !== null) {
            imagedestroy($this->image);
        }

        $this->filename = $filename;
        $this->image = $newImageResource;

        return $this;
    }

    /**
     * Provides the format-specific way to open a file.
     * E.g.- An extending class for JPEGs would open images using "imaagecreatefromjpeg()".
     *
     * @param $filename String the image file to open.
     */
    abstract protected function createFromFile(string $filename);

    /**
     * Outputs this file's contents to the standard output.
     * The extending class is expected to send the relevant content-type header.
     */
    abstract public function render() : Image;

    /**
     * Saves this image to disk.
     * The extending class implements what format the image is saved as.
     *
     * @param $filename String The new filename (or full path). If none is given then the
     * original file will be overwritten.
     */
    abstract public function save(string $filename = '') : Image;

    /**
     * Constructs an image.
     *
     * @param $filename String The image file to open.
     */
    public function __construct($filename)
    {
        $this->open($filename);
    }

    /**
     * Destroys the in-memory image data.
     */
    public function __destruct()
    {
        imagedestroy($this->image);
    }

    /**
     * Applies an image operation to this image.
     *
     * @param  $operation \Image\Contracts\Operation valid image operation.
     * @return \Image\Image This image.
     */
    public function applyOperation(Operation $operation) : Image
    {
        $this->image = $operation->apply($this->image);
        return $this;
    }

    /**
     * Gets the filename of this image.
     *
     * @return String The filename of this image.
     */
    public function getFilename() : string
    {
        return $this->filename;
    }

    /**
     * Gets the size in bytes of this image.
     *
     * @return integer The size in bytes of this image.
     */
    public function getSize() : int
    {
        return filesize($this->filename);
    }

    /**
     * Gets the width in pixels of this image.
     *
     * @return integer The width in pixels of this image.
     */
    public function getWidth() : int
    {
        return imagesx($this->image);
    }

    /**
     * Gets the height in pixels of this image.
     *
     * @return integer The height in pixels of this image.
     */
    public function getHeight() : int
    {
        return imagesy($this->image);
    }
}
