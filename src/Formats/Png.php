<?php declare(strict_types=1);

namespace Image\Formats;

use Image\Image;
use Image\Exceptions\ImageException;
use Image\Exceptions\FailedRenderException;
use Image\Exceptions\BadFileFormatException;

class Png extends Image
{
    protected $quality = 0.75;
    
    protected function createFromFile(string $filename)
    {
        $newImageResource = imagecreatefrompng($filename);

        if ($newImageResource === false) {
            throw new BadFileFormatException(
                'Could not open image file "' . $filename . '" as PNG'
            );
        }

        return $newImageResource;
    }
    
    public function getQuality() : float
    {
        return $this->quality;
    }
    
    public function setQuality(float $quality) : Image
    {
        if (!is_numeric($quality) or $quality < 0 or $quality > 1) {
            throw new ImageException(
                'Quality must be a number between 0 and 1'
            );
        }
        
        $this->quality = $quality;

        return $this;
    }
    
    public function save(string $filename = '') : Image
    {
        $failed = false;

        if ($filename) {
            $failed = !imagepng($this->image, $filename, $this->pngQuality());
        } else {
            $failed = !imagepng(
                $this->image,
                $this->filename,
                $this->pngQuality()
            );
        }

        if ($failed) {
            throw new ImageException('Could not save image');
        }

        return $this;
    }
    
    public function render() : Image
    {
        header('Content-Type: image/png');

        if (!imagepng($this->image, $this->pngQuality(), null)) {
            throw new FailedRenderException(
                'Failed rendering the image to output'
            );
        }

        return $this;
    }
    
    protected function pngQuality() : int
    {
        $quality = $this->quality * 10;
        $quality = ceil($quality);
        $quality--;
        
        return $quality;
    }
}
