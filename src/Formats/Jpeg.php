<?php declare(strict_types=1);

namespace Image\Formats;

use Image\Image;
use Image\Exceptions\ImageException;
use Image\Exceptions\FailedRenderException;
use Image\Exceptions\BadFileFormatException;

class Jpeg extends Image
{
    protected $quality = 0.75;
    
    protected function createFromFile(string $filename)
    {
        $newImageResource = imagecreatefromjpeg($filename);

        if ($newImageResource === false) {
            throw new BadFileFormatException(
                'Could not open image file "' . $filename . '" as JPEG'
            );
        }

        return $newImageResource;
    }
    
    public function getQuality() : float
    {
        return $this->quality;
    }
    
    public function setQuality(float $quality) : Image
    {
        if (!is_numeric($quality) or $quality < 0 or $quality > 1) {
            throw new ImageException(
                'Quality must be a number between 0 and 1'
            );
        }
        
        $this->quality = $quality;

        return $this;
    }
    
    public function save(string $filename = '') : Image
    {
        $failed = false;

        if ($filename) {
            $failed = !imagejpeg(
                $this->image,
                $filename,
                $this->jpegQuality()
            );
        } else {
            $failed = !imagejpeg(
                $this->image,
                $this->filename,
                $this->jpegQuality()
            );
        }

        if ($failed) {
            throw new ImageException('Could not save image');
        }

        return $this;
    }
    
    public function render() : Image
    {
        header('Content-Type: image/jpeg');

        if (!imagejpeg($this->image, null, $this->jpegQuality())) {
            throw new FailedRenderException(
                'Failed rendering the image to output'
            );
        }

        return $this;
    }
    
    protected function jpegQuality() : int
    {
        return (int)($this->getQuality() * 100);
    }
}
