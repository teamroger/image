<?php declare(strict_types=1);

namespace Image\Operations;

use Image\Contracts\Operation;

class NullOp implements Operation
{
    public function apply($image)
    {
        return $image;
    }
}
