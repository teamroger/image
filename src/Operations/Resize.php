<?php declare(strict_types=1);

namespace Image\Operations;

use Image\Contracts\Operation;
use Image\Exceptions\OperationException;

class Resize implements Operation
{
    private $newWidth = 1;
    private $newHeight = 1;
    
    public function __construct(int $newWidth, int $newHeight)
    {
        if ($newWidth < 1) {
            throw new OperationException('Width must be greater than zero');
        }

        if ($newHeight < 1) {
            throw new OperationException('Height must be greater than zero');
        }
        
        $this->newWidth = $newWidth;
        $this->newHeight = $newHeight;
    }
    
    public function apply($imageResource)
    {
        $newImage = imagecreatetruecolor($this->newWidth, $this->newHeight);
        $colour = imageColorAllocate($newImage, 255, 255, 255);
        imagefill($newImage, 0, 0, $colour);
        
        if (imagecopyresampled(
            $newImage,
            $imageResource,
            0,
            0,
            0,
            0,
            $this->newWidth,
            $this->newHeight,
            imagesx($imageResource),
            imagesy($imageResource)
        )) {
            return $newImage;
        } else {
            throw new OperationException(
                'Failed to apply Resize operation to image'
            );
        }
    }
}
