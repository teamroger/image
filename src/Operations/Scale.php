<?php declare(strict_types=1);

namespace Image\Operations;

use Image\Contracts\Operation;
use Image\Exceptions\OperationException;

class Scale implements Operation
{
    private $factor = 1;
    
    public function __construct(float $factor)
    {
        $this->factor = $factor;
    }
    
    public function apply($imageResource)
    {
        $width = imagesx($imageResource);
        $height = imagesy($imageResource);
        $newWidth = (int)($this->factor * $width);
        $newHeight = (int)($this->factor * $height);
        
        $newImage = imagecreatetruecolor($newWidth, $newHeight);
        $colour = imageColorAllocate($newImage, 255, 255, 255);
        imagefill($newImage, 0, 0, $colour);
        
        if (imagecopyresampled(
            $newImage,
            $imageResource,
            0,
            0,
            0,
            0,
            $newWidth,
            $newHeight,
            $width,
            $height
        )) {
            return $newImage;
        } else {
            throw new OperationException(
                'Failed to apply Scale operation to image'
            );
        }
    }
}
