<?php declare(strict_types=1);

namespace Image\Operations;

use Image\Contracts\Operation;
use Image\Exceptions\OperationException;

class ScaleToFit implements Operation
{
    private $newWidth = 1;
    private $newHeight = 1;
    private $margin = 0;
    
    public function __construct(int $newWidth, int $newHeight, int $margin = 0)
    {
        if ($newWidth < 1) {
            throw new OperationException('Width must be greater than zero');
        }

        if ($newHeight < 1) {
            throw new OperationException('Height must be greater than zero');
        }

        $this->newWidth = $newWidth;
        $this->newHeight = $newHeight;
        $this->margin = $margin;
    }
    
    public function apply($imageResource)
    {
        $width = imagesx($imageResource);
        $height = imagesy($imageResource);
        
        $newImage = imagecreatetruecolor($this->newWidth, $this->newHeight);
        $colour = imageColorAllocate($newImage, 255, 255, 255);
        imagefill($newImage, 0, 0, $colour);
        
        if ($width > $height) {
            $newWidth = floor($this->newWidth - $this->margin * 2);
            $newHeight = floor($this->newWidth / $width * $height);
            $newX = $this->margin;
            $newY = floor(($this->newHeight - $newHeight) / 2);
        } else {
            $newHeight = floor($this->newHeight - $this->margin * 2);
            $newWidth = floor($height / $height * $width);
            $newY = $this->margin;
            $newX = floor(($this->newWidth - $newWidth) / 2);
        }
        
        if (imagecopyresampled(
            $newImage,
            $imageResource,
            $newX,
            $newY,
            0,
            0,
            $newWidth,
            $newHeight,
            $width,
            $height
        )) {
            return $newImage;
        } else {
            throw new OperationException(
                'Failed to apply ScaleToFit operation to image'
            );
        }
    }
}
