<?php declare(strict_types=1);

namespace Image\Operations;

use Image\Contracts\Operation;
use Image\Exceptions\OperationException;

class Crop implements Operation
{
    private $x1 = 0;
    private $y1 = 0;
    private $x2 = 1;
    private $y2 = 1;
    
    public function __construct(int $x1, int $y1, int $x2, int $y2)
    {
        if ($x1 < 0 || $y1 < 0) {
            throw new OperationException(
                'Negaive coordinates are not allowed'
            );
        }

        if ($x1 >= $x2) {
            throw new OperationException('Cropped width cannot be negative');
        }

        if ($y1 >= $y2) {
            throw new OperationException('Cropped height cannot be negative');
        }

        $this->x1 = $x1;
        $this->y1 = $y1;
        $this->x2 = $x2;
        $this->y2 = $y2;
    }
    
    public function apply($imageResource)
    {
        $xdiff = $this->x2 - $this->x1;
        $ydiff = $this->y2 - $this->y1;

        $newImage = imagecreatetruecolor(
            $xdiff,
            $ydiff
        );
        $colour = imageColorAllocate($newImage, 255, 255, 255);
        imagefill($newImage, 0, 0, $colour);
        
        if (imagecopy(
            $newImage,
            $imageResource,
            0,
            0,
            $this->x1,
            $this->y1,
            $xdiff,
            $ydiff
        )) {
            return $newImage;
        } else {
            throw new OperationException(
                'Failed to apply Crop operation to image'
            );
        }
    }
}
