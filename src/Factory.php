<?php declare(strict_types=1);

namespace Image;

use Image\Formats\Png;
use Image\Formats\Jpeg;
use Image\Exceptions\BadFileFormatException;

/**
 * Creates concrete image instances.
 */
class Factory
{
    /**
     * Creates the apropriate image object based on the given filename.
     *
     * @param  $filename string The image filename.
     * @return \Image\Image An image object if the file
     *         is a recognised image otherwise NULL.
     * @throws \Image\Exceptions\BadeFileFormatException If the file type is
     *         unsupported.
     */
    public function create(string $filename) : Image
    {
        $extension = strtolower(
            substr(
                $filename,
                strrpos($filename, '.') + 1
            )
        );

        switch ($extension) {
            case 'jpg':
            case 'jpeg':
                return new Jpeg($filename);
            
            case 'png':
                return new Png($filename);
            
            default:
                throw new BadFileFormatException(
                    'File type "' . $extension . '" not supported'
                );
        }
    }
}
